package ident

import (
	"io/ioutil"
	"math/rand"
	"testing"
)

func BenchmarkIconWriteSVG(b *testing.B) {
	var err error
	for i := 0; i < b.N; i++ {
		err = Icon(rand.Int31()).WriteSVG(ioutil.Discard, 256)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkIconWritePNG(b *testing.B) {
	var err error
	for i := 0; i < b.N; i++ {
		err = Icon(rand.Int31()).WritePNG(ioutil.Discard, 256)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkIconWriteGIF(b *testing.B) {
	var err error
	for i := 0; i < b.N; i++ {
		err = Icon(rand.Int31()).WriteGIF(ioutil.Discard, 256)
		if err != nil {
			b.Fatal(err)
		}
	}
}
