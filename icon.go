/*
Package ident provides functions for rendering 8x8 identicons from 32-bit
integers in both vector and raster formats.

To use this package, first convert the data to be represented as an identicon
to a uint32. For example, if you have an IPv4 address:

	ip := net.ParseIP("127.0.0.1") // there's no place like it
	i := ident.Icon((ip[12] << 24) | (ip[13] << 16) | (ip[14] << 8) | ip[15])

Or if you have arbitrary byte data, consider a checksum:

	data := []byte("The quick red fox jumps over the lazy brown dog.")
	i := ident.Icon(crc32.ChecksumIEEE(data))

You can now render i to any io.Writer. For example, to send an SVG to standard
output:

	err := i.WriteSVG(os.Stdout, 256)

This package renders 8x8 identicons with horizontal symmetry. As such, if the
given size is not evenly divisible by eight, it is rounded down until it is.
An identicon can be no smaller than eight pixels.

The algorithm employed by this library uses three bytes of the input to
determine the block layout and the remaining byte to determine the color. Put
another way, each block pattern appears 256 times with a slightly different
color.
*/
package ident

import (
	"image"
	"image/color"
	"image/gif"
	"image/png"
	"io"
	"strconv"
)

// Icon holds the 32-bit value to be turned into an identicon. Use the Write
// methods on this type to actually render it.
type Icon uint32

// Foreground returns the icon's foreground (light) color.
func (i Icon) Foreground() (c color.RGBA) {
	c = color.RGBA{
		R: 88 + 48*i.r(),
		G: 96 + 22*i.g(),
		B: 104 + 20*i.b(),
		A: 255,
	}
	return
}

// Background returns the icon's background (dark) color.
func (i Icon) Background() (c color.RGBA) {
	c = color.RGBA{
		R: 16 + 16*i.r(),
		G: 16 + 8*i.g(),
		B: 16 + 8*i.b(),
		A: 255,
	}
	return
}

// WriteGIF renders the icon in GIF format at the given size to w.
func (i Icon) WriteGIF(w io.Writer, size int) (err error) {
	err = i.rasterize(w, size, func(w io.Writer, m image.Image) error {
		return gif.Encode(w, m, nil)
	})
	return
}

// WritePNG renders the icon in PNG format at the given size to w.
func (i Icon) WritePNG(w io.Writer, size int) (err error) {
	err = i.rasterize(w, size, func(w io.Writer, m image.Image) error {
		return png.Encode(w, m)
	})
	return
}

type direction uint8

const (
	north direction = 1
	east  direction = 2
	south direction = 4
	west  direction = 8
)

// WriteSVG renders the icon in SVG format at the given size to w.
//
// This is the fastest rendering method, but at small sizes it produces a
// larger uncompressed file than both GIF and PNG.
func (i Icon) WriteSVG(w io.Writer, size int) (err error) {
	imgSize, rowSize := clampSize(size)
	sizeStr := []byte(strconv.Itoa(imgSize))
	rowStrs := [9][]byte{
		[]byte("0"),
		[]byte(strconv.Itoa(rowSize)),
		[]byte(strconv.Itoa(2 * rowSize)),
		[]byte(strconv.Itoa(3 * rowSize)),
		[]byte(strconv.Itoa(4 * rowSize)),
		[]byte(strconv.Itoa(5 * rowSize)),
		[]byte(strconv.Itoa(6 * rowSize)),
		[]byte(strconv.Itoa(7 * rowSize)),
		[]byte(strconv.Itoa(8 * rowSize)),
	}
	bg, fg := i.Background(), i.Foreground()
	wr := &svgWriter{w: w}
	wr.write(tagSVG)
	wr.write(attWidth)
	wr.write(sizeStr)
	wr.write(quote)
	wr.write(attHeight)
	wr.write(sizeStr)
	wr.write(quote)
	wr.write(bracket)
	wr.write(tagRect)
	wr.write(attX)
	wr.write(rowStrs[0])
	wr.write(quote)
	wr.write(attY)
	wr.write(rowStrs[0])
	wr.write(quote)
	wr.write(attWidth)
	wr.write(sizeStr)
	wr.write(quote)
	wr.write(attHeight)
	wr.write(sizeStr)
	wr.write(quote)
	wr.write(attFill)
	wr.write([]byte(strconv.Itoa(int(bg.R))))
	wr.write(comma)
	wr.write([]byte(strconv.Itoa(int(bg.G))))
	wr.write(comma)
	wr.write([]byte(strconv.Itoa(int(bg.B))))
	wr.write(paren)
	wr.write(quote)
	wr.write(slash)
	wr.write(bracket)
	wr.write(tagPath)
	wr.write(attFill)
	wr.write([]byte(strconv.Itoa(int(fg.R))))
	wr.write(comma)
	wr.write([]byte(strconv.Itoa(int(fg.G))))
	wr.write(comma)
	wr.write([]byte(strconv.Itoa(int(fg.B))))
	wr.write(paren)
	wr.write(quote)
	wr.write(attData)

	// Recursively determine the edges of each subpath. edges keeps track of
	// which sides of each of the 64 squares have had an edge drawn. The values
	// of each element are a bitmask composed of one of the four direction
	// values. Thus if edges[0] is 0xf, it means that we've completely surrounded
	// the top-left square with edges.
	var edges [64]direction
	// seg is a helper for writing "M{x},{y}" and "L{x},{y}" sequences, which
	// we'll make extensive use of as we build up the SVG path definition. We
	// determine the coordinates based on the direction we're "looking at". If
	// we're looking to the west, we draw the point at the lower left of the
	// current square (identified by x and y). If we're looking to the north, we
	// draw the point at the upper left of the current square, and so on
	// clockwise around the compass. The point drawn is always to the left of the
	// vector starting in the middle of the current square and proceeding in the
	// direction d and on the edge intersected by the same vector.
	seg := func(c byte, x, y int, d direction) {
		wr.write(space)
		wr.write([]byte{c})
		wr.write(rowStrs[x+int(((d&east)>>1)|((d&south)>>2))])
		wr.write(comma)
		wr.write(rowStrs[y+int(((d&west)>>3)|((d&south)>>2))])
	}
	// draw is the function that will actually trace out the subpath. It receives
	// the starting square and a look direction, which determines the first side
	// of the square it looks at to see whether it's an edge. It returns true for
	// closed when it encounters a square that has an edge on the side that is
	// one step anticlockwise from the current look direction.
	var draw func(int, int, direction) bool
	draw = func(x, y int, d direction) (closed bool) {
		// j is the index into edges that refers to the current square.
		j := (8 * y) + x
		// Starting with the look direction, we proceed clockwise around the
		// compass, checking each side of the current square.
		for k := 0; k < 4; k, d = k+1, d<<1 {
			// Reset the direction back to north if we shifted d to an invalid value.
			if d > west {
				d = north
			}
			// dx and dy refer to the square next to the current squared in the
			// direction d. These coordinates could be beyond the canvas boundaries.
			// That's OK, because the Icon.on method will treat all out-of-bounds
			// coordinates as "off".
			dx := x + int((d&east)>>1) - int((d&west)>>3)
			dy := y + int((d&south)>>2) - int((d&north)>>0)
			// We know that we should draw an edge if the current square (x, y) is "on"
			// and the neighboring square (dx, dy) is "off".
			if edge := i.on(x, y) && !i.on(dx, dy); edge {
				// If we've already drawn this edge, there's no reason to proceed.
				if edges[j]&d != 0 {
					continue
				}
				// Otherwise we add this direction to the appropriate bitmask in edges
				// to signal to future invocations that we've already drawn this edge
				// and use seg to record the next point in the subpath.
				edges[j] |= d
				seg('L', x, y, d)
			} else if i.on(dx, dy) {
				// If the neighboring square is also "on", we recurse into it to
				// continue the path. When advancing to a neighboring square, we want
				// to start looking in the direction one step anticlockwise from d. For
				// example, when stepping into a square to the east, we want to start
				// looking at the north edge. When stepping to the south, we want to
				// start looking to the east. This pattern ensures that we don't miss
				// any edges of the subpath.
				e := d >> 1
				if e == 0 {
					e = west
				}
				// If we see that the first edge to be examined in the next square has
				// already been drawn, we know we've successfully reached the end of
				// this subpath. We close the subpath with a "Z" segment and set the
				// return value to true so that callers further up the stack can exit
				// early.
				if edges[(8*dy)+dx]&e != 0 {
					wr.write(space)
					wr.write([]byte{'Z'})
					closed = true
					return
				}
				// Otherwise we recurse. If we find out that the subpath has been
				// closed, there's no reason to examine any other edges of the current
				// square.
				closed = draw(dx, dy, e)
				if closed {
					return
				}
			}
		}
		return
	}
	// Starting from the top-left corner and proceeding left-to-right,
	// top-to-bottom, we examine each square that's "on". For each such square,
	// if the square to the west is "off" and we have not yet drawn an edge to
	// the west, we start a new subpath. Otherwise, if the square to the east is
	// "off" and we have not yet drawn an edge to the east, we have found a
	// subpath enclosed by a larger subpath, so we also start a new subpath here.
	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			if !i.on(x, y) {
				continue
			}
			if !i.on(x-1, y) && edges[(8*y)+x]&west == 0 {
				seg('M', x, y, west)
				draw(x, y, west)
			}
			if !i.on(x+1, y) && edges[(8*y)+x]&east == 0 {
				seg('M', x, y, east)
				draw(x, y, east)
			}
		}
	}
	wr.write(quote)
	wr.write(slash)
	wr.write(bracket)
	wr.write(tagSVGClose)
	return
}

// r returns the red component of the color.
func (i Icon) r() (r uint8) {
	r = uint8((i & 0x8) >> 3)
	r |= uint8((i & 0x8000) >> 14)
	return
}

// g returns the green component of the color.
func (i Icon) g() (g uint8) {
	g = uint8((i & 0x80) >> 7)
	g |= uint8((i & 0x80000) >> 18)
	g |= uint8((i & 0x8000000) >> 25)
	return
}

// b returns the blue component of the color.
func (i Icon) b() (b uint8) {
	b = uint8((i & 0x800) >> 11)
	b |= uint8((i & 0x800000) >> 22)
	b |= uint8((i & 0x80000000) >> 29)
	return
}

// clampSize ensures that the given size is greater than or equal to eight and
// evenly divisible by eight. Any adjustments made to size are returned as s,
// and r (the row size) is set to s divided by eight.
func clampSize(size int) (s int, r int) {
	s = size
	if s < 8 {
		s = 8
	}
	s &= ^0x7
	r = s >> 3
	return
}

var blockOffsets = [64]uint8{
	0:  28,
	1:  28,
	2:  24,
	3:  24,
	4:  24,
	5:  24,
	6:  28,
	7:  28,
	8:  28,
	9:  28,
	10: 24,
	11: 24,
	12: 24,
	13: 24,
	14: 28,
	15: 28,
	16: 20,
	17: 20,
	18: 16,
	19: 16,
	20: 16,
	21: 16,
	22: 20,
	23: 20,
	24: 20,
	25: 20,
	26: 16,
	27: 16,
	28: 16,
	29: 16,
	30: 20,
	31: 20,
	32: 12,
	33: 12,
	34: 8,
	35: 8,
	36: 8,
	37: 8,
	38: 12,
	39: 12,
	40: 12,
	41: 12,
	42: 8,
	43: 8,
	44: 8,
	45: 8,
	46: 12,
	47: 12,
	48: 4,
	49: 4,
	50: 0,
	51: 0,
	52: 0,
	53: 0,
	54: 4,
	55: 4,
	56: 4,
	57: 4,
	58: 0,
	59: 0,
	60: 0,
	61: 0,
	62: 4,
	63: 4,
}

var blockPatterns = [8]uint8{
	0: 0x9, // ▚ (upper left + lower right)
	1: 0x5, // ▌ (upper left + lower left)
	2: 0xb, // ▜ (upper left + upper right + lower right)
	3: 0xe, // ▟ (upper right + lower left + lower right)
	4: 0xd, // ▙ (upper left + lower left + lower right)
	5: 0x7, // ▛ (upper left + upper right + lower left)
	6: 0xa, // ▐ (upper right + lower right)
	7: 0x6, // ▞ (upper right + lower left)
}

// on determines whether the "pixel" at coordinate (x, y) is "turned on" (drawn
// with the foreground color). Coordinates that are outside the bounds of the
// image are are considered to be "turned off".
func (i Icon) on(x, y int) bool {
	nx, ny := x&0x7, y&0x7
	if x != nx || y != ny {
		return false
	}
	offset := blockOffsets[8*ny+nx]
	block := int(i&(0x7<<offset)) >> offset
	block += int((28 - offset) >> 2)
	block &= 0x7
	if nx > 3 {
		block ^= 0x7
	}
	mask := uint8(8)
	mask >>= uint(nx & 1)
	mask >>= uint(ny&1) << 1
	return blockPatterns[block]&mask != 0
}

// rasterize creates a paletted image for the icon and uses the given encoder
// function to write it to w.
func (i Icon) rasterize(w io.Writer, size int, encode func(io.Writer, image.Image) error) (err error) {
	imgSize, rowSize := clampSize(size)
	bg, fg := i.Background(), i.Foreground()
	p := color.Palette{bg, fg}
	m := image.NewPaletted(image.Rectangle{Max: image.Point{imgSize, imgSize}}, p)
	defer func() { err = encode(w, m) }()
	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			ci := uint8(0)
			if i.on(x, y) {
				ci = 1
			}
			for my, ylim := rowSize*y, rowSize*(y+1); my < ylim; my++ {
				for mx, xlim := rowSize*x, rowSize*(x+1); mx < xlim; mx++ {
					m.Pix[my*imgSize+mx] = ci
				}
			}
		}
	}
	return
}

var (
	tagSVG      = []byte(`<svg version="1.1" xmlns="http://www.w3.org/2000/svg"`)
	tagSVGClose = []byte(`</svg>`)
	tagPath     = []byte(`<path`)
	tagRect     = []byte(`<rect`)
	attData     = []byte(` d="`)
	attHeight   = []byte(` height="`)
	attFill     = []byte(` fill="rgb(`)
	attWidth    = []byte(` width="`)
	attX        = []byte(` x="`)
	attY        = []byte(` y="`)
	space       = []byte(` `)
	quote       = []byte(`"`)
	comma       = []byte(`,`)
	paren       = []byte(`)`)
	bracket     = []byte(`>`)
	slash       = []byte(`/`)
)

type svgWriter struct {
	w   io.Writer
	err error
}

func (w *svgWriter) write(b []byte) {
	if w.err != nil {
		return
	}
	_, w.err = w.w.Write(b)
}
